"use strict";

require("./select_field.less");

var React = require("react");
var ReactDom = require("react-dom");
var $ = require("jquery");
var SelectInput = require("../select_input/select_input.jsx");
var Icon = require("../icon/icon.jsx");


class SelectField extends React.Component{
	constructor(props){
		super();

		this.updateState = this.updateState.bind(this);
		this.heared = this.heared.bind(this);
		this.fetchFromServer = this.fetchFromServer.bind(this);
		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
		this.setValue = this.setValue.bind(this);
		this.changed = this.changed.bind(this);
		this.cancel = this.cancel.bind(this);
		this.clear = this.clear.bind(this);

		this.state =  {
			is_closed:true,
			value: props.value || null,
			text:"",
			data:null,
			title: props.title || "",
			fk: props.fk || null,
			popup_element:null,
			enabled:true
		}
	}

	componentWillMount(){
		// навешиваем слушателя
		if (this.props.listen){
			$(window).on(this.props.listen, this.heared);
		}
	}

	componentDidMount(){
		// пересчитываем высоту элемента
		this.item_height = ReactDom.findDOMNode(this.refs.choice).offsetHeight;
	}

	updateState(state){
		// изменяем стэйт и генерируем событие
		var old_state = this.state.value;
		this.setState(state);
		if (this.state.value != old_state)
		{
			if (this.props.emit){
				$(window).trigger(this.props.emit,{"name":this.state.title,"id":this.state.value});
			}
		}
	}

	heared(e,data){
		// если возникло событие, которое слушаем:
		this.updateState({title:"",value:null, fk:data.id, data:null})
		if (this.props.url){
			setTimeout(function(){this.fetchFromServer()}.bind(this), 10);
		}
	}

	fetchFromServer(){
		var url = this.props.url;
		if (this.state.fk){
			url = url + "/"+this.state.fk + "/";
		}
		$.ajax({
	      url: url,
	      dataType: 'json',
	      success: function(data) {
	      	var state = {'data': data.result, "enabled": false};
	      	if (data.result){
	      		if (data.result.length>0){
	      			state.enabled = true;
	      		}
	      	}
	        this.updateState(state);
	        if (this.state.popup_element){
	        	this.state.popup_element.updateData(data.result);
	        }
	      }.bind(this),
	      error: function(xhr, status, err) {
	        console.log(status, err.toString());
	      }.bind(this)
	  });
	}

	open(){
		var this_node = $(ReactDom.findDOMNode(this));
		var offset = this_node.offset();
		var left = offset.left;
		var top = offset.top;
		console.log("TOP:", top);
		var width = this_node.width();
		// var data = this.filter_result();
		var data = this.state.data;

		var this_choice = ReactDom.findDOMNode(this.refs.choice);
		this.item_height = this_choice.offsetHeight;

		var popup = ReactDom.render(
		  	<SelectInput item_height={this.item_height} left={left} top={top} width={width} data={data} value={this.state.value} title={this.state.title} onChange={this.changed} onCancel={this.cancel}/> ,
		  	document.getElementById('popup_container')
		);
		this.setState({popup_element:popup});
		if (this.state.data==null){
			this.fetchFromServer();
		}
		if (this.state.is_closed){
			this.setState({is_closed:false});

			this.width = this_node.width();
			this.left = 0;
			this.top = $(this_choice).height()+4;
		}
	}

	close(){
		if (!this.state.is_closed){
			this.setState({is_closed:true});
		}
		if (this.state.popup_element){
			var node = ReactDom.findDOMNode(this.state.popup_element);
  			ReactDom.unmountComponentAtNode(node.parentNode);
			$(node).remove();
			this.setState({popup_element:null});
		}
	}

	setValue(val){
		this.updateState({value:val});
	}

	changed(item){
		this.close();
		this.updateState({title:item.name,value:item.id});
		if (this.props.onChange) this.props.onChange(item);

	}

	cancel(){
		this.close();
	}

	clear(e){
		e.stopPropagation();
		this.updateState({
			is_closed:true,
			value:null,
			text:"",
			title:""
		});

	}

	render(){
		var root_classname = "selectfield";

		if (!this.state.enabled){
			return (
			<div className="selectfield disabled" tabIndex="0" data-enabled={this.state.enabled}>
				<div className="selectfield__choicewrapper">
				<span className="selectfield__choice" ref="choice">
					<span className="selectfield__text">{this.state.title}</span>
				</span>
				</div>
				<input id={this.props.id} name={this.props.name} value={this.state.value} type="hidden" />
			</div>

			);

		}

		var clr = false;
		var icon_expand = <Icon path="expand-more" no-event="true" />
		if (this.state.value && this.state.value!=-1){
			clr = <a className="selectfield__icon pointer" onClick={this.clear} title="Очистить"><i className="md md-clear"></i></a>
			icon_expand = false;
		}
		return (
			<div className="selectfield" tabIndex="0" data-enabled={this.state.enabled}>
				<div className="selectfield__choicewrapper pointer">
				<span className="selectfield__choice" onClick={this.open} ref="choice">
					<span className="selectfield__text">{this.state.title}</span>
					{icon_expand}
				</span>


				{clr}
				</div>

				<input id={this.props.id} name={this.props.name} value={this.state.value} type="hidden" />
			</div>

		);
	}
}

module.exports = SelectField;
