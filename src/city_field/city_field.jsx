"use strict";


require("./city_field.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");

var Popup = require("../popup/popup.jsx");
var CitiesList = require("../cities_list/cities_list.jsx");
var Icon = require("../icon/icon.jsx");
var InputElement = require("../input_element/input_element.jsx");

/*
value
title
url
emit
name
required
*/
class CityField extends React.Component{
	constructor(props){
		super();

		this.showPopup = this.showPopup.bind(this);
		this.closePopup = this.closePopup.bind(this);
		this.onSelect = this.onSelect.bind(this);

		this.state = {
			 value: props.value,
			 title: props.title,
			}
	}

	showPopup(){
		var popup = <Popup title="Выберите город" maxWidth="1080">
						<CitiesList  onSelect={this.onSelect}  city={this.state.value} url={this.props.url} />
					</Popup>
		this.popup = ReactDom.render(	popup, document.getElementById('popup_container'));
	}

	closePopup(){
		if (this.popup){
			this.popup.remove_me();
			this.popup = null;
		}
	}

	onSelect(value){
		setTimeout(this.closePopup,10);
		var st = {};
		st.value = value.id|0;
		st.title = value.name;
		this.setState(st);
		if (this.props.emit){
			$(window).trigger(this.props.emit, value);
		}
		$(this.refs.inp).trigger("change");

	}

	render(){
		if (this.props.required){
		}
		return (
			<div className="cityfield">
				<InputElement title={this.state.title} isValid={this.props.isValid} onClick={this.showPopup} name={this.props.name} value={this.state.value} icon="expand-more" />					
			</div>
		);
	}
}

module.exports = CityField;
