"use strict";

require("./login_form.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");
var Button = require("../button/button.jsx");

class LoginForm extends React.Component{
	constructor(props){
		super();

		this.state = {
			"login": props.login || "/login/",
			"register": props.register || "/register/",
			"recover": props.recover || "/reset_pass/"
		}
	}
	render(){
		return (
			<div className="form">
				<div className="row form_line">
					<div className="col-md-2">
						<label className="control-label" htmlFor="reg_name">Email</label>
					</div>
					<div className="col-md-10">
						<input type="text" name="email" placeholder="Email" className="loginform__field" />
					</div>
				</div>
				<div className="row form_line">
					<div className="col-md-2">
						<label className="control-label" htmlFor="reg_name">Пароль</label>
					</div>
					<div className="col-md-10">
						<input type="password" name="password" placeholder="Пароль" className="loginform__field" />
					</div>
				</div>
				<div className="buttonList buttonList__mod_right">
					<Button mods="success">Войти</Button>
				</div>

				<hr/>

				<div className="loginform__help">
					<a href={this.props.register}>Зарегистрироваться</a> - <a href={this.props.recover}>Восстановить пароль</a>
				</div>
			</div>
		)
	}
}


module.exports = LoginForm;
