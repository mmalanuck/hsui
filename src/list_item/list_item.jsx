"use strict";

require("./list_item.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");


/*
onClick
name
selected
value
left_icon={icon}
right_icon
badge
decoration={decor}
show_name=false
*/

class ListItem extends React.Component{
	constructor(props){
		super();
		this.onClick = this.onClick.bind(this);
	}

	onClick(){
		if (this.props.onClick){
				this.props.onClick({
					value:this.props.value,
					name: this.props.name
				});
		}
	}

	// data-id={item.id}
	// data-name={item.name}

	render(){
		var class_name = "list_item";
		if (this.props.selected){
			class_name += " list_item__active"
		}

		if (this.props.mods){
			var mods_res = _.reduce(this.props.mods.split(" "), (res, item)=>{
				return res+=" list_item__mod__"+item;
			}, "")
			class_name += mods_res;
		}


		var content = this.props.children;
		if (this.props.showName=="true"){
			content = this.props.name;
		}
		return (
      <div className={class_name}
				onClick={this.onClick}>{content}</div>
		);

	}
}


module.exports = ListItem;
