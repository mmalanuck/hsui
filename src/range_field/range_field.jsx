"use strict";

require("./range_field.less");

var React = require("react");
var ReactDom = require("react-dom");
var $ = require("jquery");

/*
max
min
value_a
value_b
name_a
name_b
*/

function pauseEvent(e){
    if(e.stopPropagation) e.stopPropagation();
    if(e.preventDefault) e.preventDefault();
    e.cancelBubble=true;
    e.returnValue=false;
    return false;
}

class RangeField extends React.Component{
	constructor(props){
		super();

		this.drag_a_start = this.drag_a_start.bind(this);
		this.drag_b_start = this.drag_b_start.bind(this);
		this.drag_a_move = this.drag_a_move.bind(this);
		this.drag_b_move = this.drag_b_move.bind(this);
		this.drag_a_end = this.drag_a_end.bind(this);
		this.drag_b_end = this.drag_b_end.bind(this);

		var max_val = props.max || 5000;
		var min_val = props.min || 0;

    var val_a = props.valueA || min_val;
    var val_b = props.valueB || max_val;
    if (val_a == -1){
      val_a = min_val;
    }
    if (val_b == -1){
      val_b = max_val;
    }


		this.state = {
			 min: min_val,
			 value_a: val_a,
			 max: max_val,
			 value_b: val_b,
			 size: 0
			}
	}

	componentDidMount(){
		var st = {};
		var size = this.refs.line.offsetWidth;
		st.size = size;
		this.setState(st);

		// подписываемся на мышыные события
		$(this.refs.span_a).on("mousedown", this.drag_a_start);
		$(this.refs.span_b).on("mousedown", this.drag_b_start);
	}


	drag_a_start(e){
		this.move_a_handler = this.drag_a_move;
		this.up_a_handler = this.drag_a_end;

		$(document).on("mousemove",this.move_a_handler);
        $(document).on("mouseup",this.up_a_handler);

        this.start_x = e.clientX;
        this.old_val = this.state.value_a;
        if (this.old_val == -1){
        	this.old_val = 0;
        }
	}

	drag_b_start(e){
		this.move_b_handler = this.drag_b_move;
		this.up_b_handler = this.drag_b_end;

		$(document).on("mousemove",this.move_b_handler);
        $(document).on("mouseup",this.up_b_handler);

        this.start_x = e.clientX;
        this.old_val = this.state.value_b;
        if (this.old_val == -1){
        	this.old_val = this.state.max|0;
        }

	}

	drag_a_move(e){
		pauseEvent(e);
		var step = (this.state.max - this.state.min) / this.state.size ;
		var value = this.old_val + (e.clientX - this.start_x)* step;
		if (value < this.state.min){
			value = this.state.min;
		}
		if (value > this.state.value_b){
			value = this.state.min;
		}
		value = value | 0;
		var st = {value_a:  value };
		this.setState(st);
	}

	drag_b_move(e){
		pauseEvent(e);
		var max = this.state.max;
		var min = this.state.min;

		var step = (max - min) / this.state.size ;
		var value = this.old_val | 0;
		value += (e.clientX - this.start_x)* step;
		if (value < this.state.value_a){
			value = max;
		}
		if (value > max){
			value = max;
		}
		value = value | 0;
		var st = {value_b:  value };
		this.setState(st);
	}

	drag_a_end(e){
		$(document).off("mousemove",this.move_a_handler);
		$(document).off("mouseup",this.up_a_handler);
	}

	drag_b_end(e){
		$(document).off("mousemove",this.move_b_handler);
		$(document).off("mouseup",this.up_b_handler);
	}

	render(){
		var size = this.state.size;		// размер области в пикселях

		var max = this.state.max;
		var min = this.state.min;

		var start = this.state.value_a - min;		// положение первого манипулятора
		var end = this.state.value_b - min;			// пложение второго манипулятора

		start = size/(max-min)*(start);
		end = size/(max-min)*(end);

		if (start < 0){
			start = 0;
		}

		if ((end-start)<15){
			end = start + 15;
		}

		var handler_a_style = { cursor: "pointer", left: start };
		var handler_b_style = { cursor: "pointer", 	left: end };
		var range_style = { left : start, width : end-start };

		var name_a = this.props.nameA || "value_min";
		var name_b = this.props.nameB || "value_max";
		var inp_style = {display: "none"};

		return (
			<div className="rangefield" ref="line">
				<div className="rangefield__value">{this.state.value_a}-{this.state.value_b}</div>
				<div className="rangefield__area" style={range_style} ></div>
				<span className="rangefield__handler left" style={handler_a_style} ref="span_a"></span>
				<span className="rangefield__handler right" style={handler_b_style} ref="span_b"></span>
				<input type="text" name={name_a} readOnly="true" value={this.state.value_a} style={inp_style} />
				<input type="text" name={name_b} readOnly="true" value={this.state.value_b} style={inp_style} />
			</div>
		);
	}
}

module.exports = RangeField;
