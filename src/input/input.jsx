"use strict";

require("./input.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");


// name
// placeholder
// readonly
class Input extends React.Component{
	constructor(props){
		super();

		this.onChange = this.onChange.bind(this);
		this.validate = this.validate.bind(this);

		this.state = {
			value: props.value,
			hasError: props.hasError,
			message: props.message
		}
	}
	validate(val){
		if (val.length < 4){
			return "Less than 4 symbols";
		}
		return true;
	}

	onChange(e){
		var value = e.target.value;
		var valid_message = this.validate(value);
		if (valid_message !=true ){
				this.setState({value: value, hasError: true, message: valid_message});
		}else{
			this.setState({value: value, hasError: false, message: null});
		}

		if (this.props.onChange){
				this.props.onChange(value);
		}
	}

	render(){
		var cls_name =  "input";
		if (this.state.hasError){
			cls_name +=" input_mod_error";
		}
		var message = null;
		if (this.state.hasError){
			message = <div className="input_message">{this.state.message}</div>
		}
		return (
			<div className={cls_name}>
				<input placeholder={this.props.placeholder} name={this.props.name} value={this.state.value} onChange={this.onChange}/>
				{message}
			</div>
		)
	}
}


module.exports = Input;
