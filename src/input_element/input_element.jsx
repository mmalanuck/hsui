"use strict";

require("./input_element.less");

var React = require("react");
var ReactDom = require("react-dom");
var $ = require("jquery");
var Icon = require("../icon/icon.jsx");


/*
clearable
clear_value
value
title
enabled
onClick
onChanged
field_name
name
icon
*/
class InputElement extends React.Component{
	constructor(props){
		super();

		this.click_item = this.click_item.bind(this);
		this.clear = this.clear.bind(this);

		this.state =  {
			value: props.value || props.clear_value || -1,
			title: props.title || "Не указано",
			enabled: (props.enabled || "true")=="true",
			clearable: (props.clearable || "false")=="true",
			clear_value: props.clear_value || -1,
			message: props.message || null
		}
	}
	clear(){
		this.setState({
			value: this.state.clear_value,
			title: "Не указано"
		})
		if (this.props.onChange){
			this.props.onChange(this.state.value);
		}
	}

	click_item(e){
		if (this.state.enabled){
			if (this.props.onClick){
				this.props.onClick(e);
			}
		}
	}

	render(){
		var root_classname = "input_element";
		var clr = false;

		// var icon_expand = <i className="selectfield__icon md md-expand-more no-event"></i>;
		// if (this.state.clearable && this.state.value!=undefined && this.state.value!=this.state.clear_value){
		var icon = null;
		if (this.props.icon){
			icon = <Icon path={this.props.icon} no_event="true" />
		}
		if (this.state.clearable && this.state.value!=undefined && this.state.value!=this.state.clear_value){
			clr = <a className="input_element__clear" onClick={this.clear} title="Очистить"><i className="md md-clear"></i></a>
		}
		var class_name = "input_element";
		if (this.state.enabled != true){
			class_name += " input_element__disabled"
		}
		if (this.props.isValid == false){
			class_name += " input_mod__wrong"
		}

		var message = null;
		if (this.state.message && this.state.message.length > 0){
			message = <div className="input__message">{this.state.message}</div>
		}

		return (
			<div className={class_name} tabIndex="0">
				<div className="input_element__choicewrapper">
				<span className="input_element__choice" onClick={this.click_item}>
					<span className="input_element__text">{this.props.title}</span>
					{clr}
					{icon}
				</span>

				</div>
				{message}
				<input id={this.props.id} name={this.props.name} value={this.props.value} type="hidden" />
			</div>

		);
	}
}

module.exports = InputElement;
