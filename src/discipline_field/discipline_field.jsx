"use strict";

require("./discipline_field.less");

var React = require("react");
var $ = require("jquery");
var _ = require("underscore");
var ReactDom = require("react-dom");
var Popup = require("../popup/popup.jsx");
var DisciplineList = require("../discipline_list/discipline_list.jsx");
var InputElement = require("../input_element/input_element.jsx");
/*
listen
*/

class DisciplineField extends React.Component{

	constructor(props){
		super();

		this.popup = null;

		this.heared = this.heared.bind(this);
		this.showPopup = this.showPopup.bind(this);
		this.closePopup = this.closePopup.bind(this);
		this.onSelect = this.onSelect.bind(this);
		this.updateState = this.updateState.bind(this);

		if (props.listen){
			$(window).on(this.props.listen, this.heared);
		}

		this.state = {
			value: props.value,
			subject: props.subject,
			title: props.title || "не указано",
			city: props.city || null,
		}
	}

	heared(e,data){
		// если возникло событие, которое слушаем:
		if (this.state.city != data.id){
			this.updateState({city:data.id, data:null})
		}
	}

	showPopup(){
		var popup = <Popup title="Выберите дисциплину"  maxWidth="820" removetop="true">
						<DisciplineList  onSelect={this.onSelect}  discipline={this.state.value} subject={this.state.subject} city={this.state.city} url={this.props.url} />
					</Popup>
        var popup_div = $("<div class='popups'></div>");
        $('#popup_container').append(popup_div);

		this.popup = ReactDom.render(	popup, popup_div[0]);
        this.popup_div = popup_div;

	}

	closePopup(){
		if (this.popup){
			this.popup.remove_me();
			this.popup = null;
		}
	}

	onSelect(discipline){
		this.setState({
			value : discipline.id|0,
			subject : discipline.subject|0,
			title : discipline.name
		});
		$(this.refs.inp).trigger("change");
		setTimeout(this.closePopup,10);
	}

	updateState(state){
		// изменяем стэйт и генерируем событие
		this.setState(state);
		if (this.props.emit){
			$(window).trigger(this.props.emit,{"name":this.state.title,"id":this.state.value});
		}
	}

	render(){
		return (
					<InputElement
						title={this.state.title}
						onClick={this.showPopup}
						name={this.props.name}
						value={this.state.value}
						isValid={this.props.isValid}
					/>
		);
	}
}

module.exports = DisciplineField;
