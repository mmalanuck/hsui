"use strict";

require("./button.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");

/*
onClick
mods
*/

class Button extends React.Component{
	constructor(props){
		super();
		this.onClick = this.onClick.bind(this);
	}

	onClick(e){
    e.stopPropagation();
		if (this.props.onClick){
				this.props.onClick();
		}
	}

	render(){
		var class_name = "button";
    if (this.props.mods){
      var mods_class = _.reduce(this.props.mods.split(" "), (res, item)=>{
        return res + " button__mods__"+item;
      }, "");
      class_name += mods_class;
		}
		if (this.props.no_event=="true"){
			class_name+=" no-event";
		}
		if (this.props.disabled=="true"){
			class_name+=" button__mods__disabled";
		}
		if (this.props.no_right_padding=="true"){
			class_name+=" button__mods__norightpadding";
		}
		if (this.props.no_left_padding=="true"){
			class_name+=" button__mods__noleftpadding";
		}

		var text = this.props.children;
		if (text==undefined){
			text = "Button";
		}
	   return (
      <div className={class_name} onClick={this.onClick}>{text}</div>
		);

	}
}


module.exports = Button;
