"use strict";


require("./popup_register.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");

var Popup = require("../popup/popup.jsx");
var RegisterForm = require("../register_form/register_form.jsx");

/*
city_id
city_name
user_type
*/
class PopupRegister extends React.Component{
	constructor(props){
		super();

		var city_id = 449;
		var city_name = "Курск";
		var region_id = null;
		var fo_id = null;

		this.select_two = this.select_two.bind(this);
		this.select_one = this.select_one.bind(this);

		if (window.data){
			city_id = window.data.city_id;
			city_name = window.data.city_name;
			region_id = window.data.region_id;
			fo_id = window.data.fo_id;
		}

		this.state = {
			"type_1":false,
			"type_2":true,
			"city_id":props.city_id || city_id,
			"city_name":props.city_name || city_name,
			"region_id":props.region_id || region_id,
			"fo_id":props.fo_id || fo_id
		}
	}

	select_two(){
		this.setState({"type_1":false,"type_2":true});
	}

	select_one(){
		this.setState({"type_1":true,"type_2":false});
	}

	render(){
		return (
		<Popup title="Регистрация" maxWidth="820">
			<RegisterForm />
	</Popup>
		)
	}
}


module.exports = PopupRegister;
