"use strict";


require("./image_upload.less");
var React = require("react");
var ReactDom = require("react-dom");


class ImageUpload extends React.Component{
	constructor(props){
		super();

		this.click = this.click.bind(this);
		this.cancel = this.cancel.bind(this);
		this.change = this.change.bind(this);
		this.loaded = this.loaded.bind(this);

		this.state = {
			state: 0,
			url: null
		}
	}

	componentWillMount(){
		if (this.props.url){
			this.setState({"url":this.props.url})
		}
	}
	click(){
		ReactDom.findDOMNode(this.refs.input).click();
	}
	cancel(){
		this.setState({state:0,url:this.props.url});
		var inp = $(ReactDom.findDOMNode(this.refs.input));
		var inp_clone = inp.clone();
		inp.replaceWith(inp_clone);
	}
	change(e){
		var file = e.target.files[0];
		this.setState({state:1});
		var reader = new FileReader();
		reader.onload = this.loaded;
		reader.readAsDataURL(file);
	}
	loaded(e){
		var res = e.target.result;
		this.setState({state:2,url:res});
	}

	render(){
		var top_style = {
			width:this.props.width,
			height: this.props.height,
			backgroundImage: "url("+this.state.url+")"
		}
		var button = null;
		if (this.state.state==0) button =  <div className="imageUploader__button" onClick={this.click}>Загрузить</div>
		if (this.state.state==1) button =  <div className="imageUploader__status">загружается...</div>
		if (this.state.state==2) button =  <div className="imageUploader__button" onClick={this.cancel}>Отменить</div>



		var input_style={"display":"none"};

		return (
			<div className="imageUploader" style={top_style}>
				<div className="imageUploader__bottom_panel">{button}</div>
				<input type='file' name={this.props.name} style={input_style} ref="input" onChange={this.change}></input>
			</div>
		)
	}

}


module.exports = ImageUpload;
