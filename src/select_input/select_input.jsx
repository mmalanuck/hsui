"use strict";

require("./select_input.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");

class SelectInput extends React.Component{
	constructor(props){
		super();

		this.find_selected_item = this.find_selected_item.bind(this);
		this.updateData = this.updateData.bind(this);
		this.textChange = this.textChange.bind(this);
		this.setItem = this.setItem.bind(this);
		this.close = this.close.bind(this);
		this.clickElement = this.clickElement.bind(this);
		this.keyPress = this.keyPress.bind(this);
		this.keyEsc = this.keyEsc.bind(this);
		this.keyEnter = this.keyEnter.bind(this);
		this.keyRight = this.keyRight.bind(this);
		this.keyUp = this.keyUp.bind(this);
		this.keyDown = this.keyDown.bind(this);
		this.increment = this.increment.bind(this);
		this.wheel = this.wheel.bind(this);

		this.state = {
			list: props.data || [],
			value: props.value,
			text: props.title || "",
			hovered:null,
			full_list: props.data || [],
			v_pos:0
		};
	}

	find_selected_item(text,list){
		var v_pos = 0;
		if ((text.length>0) && (list!=null) && (list.length>0)){
			var self = this;
			var lower_text = text.toLowerCase();
			_.each(list,function(el,i,lst){
				if (el.name.toLowerCase()==lower_text){
					v_pos=i;
				}
			});

		}
		return v_pos;
	}

	componentDidMount(){
		$(this.refs.inp).val(this.state.text);
		$(this.refs.inp).focus();
		$(this.refs.inp).select();

		var v_pos = this.find_selected_item(this.state.text,this.state.list);
		this.setState({"v_pos":v_pos,"hovered":v_pos,text:""});
	}

	updateData(data){
		var v_pos = this.find_selected_item(this.props.title || "",data);
		this.setState({"full_list":data,"list":data,"v_pos":v_pos,"hovered":v_pos});
	}

	textChange(e){
		var entered = $(this.refs.inp).val().toLowerCase();
		if (entered.length>0){
			var filtered = _.filter(this.state.full_list,function(a){
				return (a.name.toLowerCase().indexOf(entered))>=0;
			});

			this.setState({list:filtered,text:entered, hovered:0,v_pos:0});
		}else{
			this.setState({list:this.state.full_list,text:entered, hovered:0,v_pos:0});
		}
	}

	setItem(item){
		if(item==null) return;
		$(this.refs.inp).val(item.id);
		this.setState({text:item.name,value:item.id, hovered:null,list:[]});
		if (this.props.onChange) this.props.onChange(item);
	}

	close(e){
		this.setState({hovered:null});
		if (this.props.onCancel) this.props.onCancel();

	}

	clickElement(txt){
		this.setItem(txt);
	}

	keyPress(e){
		if (e.keyCode == 27)	this.keyEsc(e);
		if (e.keyCode == 13)	this.keyEnter(e);
		if (e.keyCode == 39)   this.keyRight(e);
		if (e.keyCode == 38)   this.keyUp(e);
		if (e.keyCode == 40)   this.keyDown(e);
	}

	keyEsc(e){
		e.preventDefault();
		this.close();
	}

	keyEnter(e){
		e.stopPropagation();
		e.preventDefault();

		if (this.state.hovered!=null){
			var entered = this.state.list[this.state.hovered];
			this.setItem(entered);
		}
	}

	keyRight(e){
		if (this.state.hovered!=null){
			e.preventDefault();
			this.keyEnter(e);
		}
	}

	keyUp(e){
		var pos = this.increment(-1);
		this.setState({hovered:pos, v_pos:pos});
		e.preventDefault();
	}

	keyDown(e){
		var pos = this.increment(1);
		this.setState({hovered:pos, v_pos:pos});
		e.preventDefault();
	}

	increment(step){
		var hov = this.state.v_pos;
		if (this.state.list.length>0){
			if (this.state.hovered!=null){
				hov = this.state.hovered+step;
			}else{
				hov = 0;
			}
			if (hov<0) hov = 0;
			if (hov>=this.state.list.length) hov = this.state.list.length-1;
		}
		return hov;
	}

	wheel(e){
		e.preventDefault();
		var pos = null;
		var val = 100.0/this.state.content_height*80;
		if (e.deltaY<0) {
			pos = this.increment(-1);
		}
		else {
			pos = this.increment(1);
		}
		this.setState({hovered:pos, v_pos:pos});
	}


	render(){
		var style = {
			maxWidth:400,
			minWidth:80,
			width:this.props.width,
			left:this.props.left,
			top:this.props.top,
			display:"block"
		};

		var variants = [];
		var lower_text = "";
		if (this.state.text) lower_text = this.state.text.toLowerCase();
		var self = this;

		_.each(this.state.list,function(el,i,lst){
			var cname = "result-item";
			if (i==self.state.hovered) cname = "result-item highlighted";
			if (el.name == undefined){
				return;
			}
			if (el.name.length>0){
				var ind = el.name.toLowerCase().indexOf(lower_text);
				var pos1 = el.name.slice(0,ind);
				var pos2 = el.name.slice(ind,ind+lower_text.length);
				var pos3 = el.name.slice(ind+lower_text.length,el.name.length);

				variants.push(<li key={i}  className={cname} onClick={function(){self.clickElement(el)}}>
							<div className="label" >
								{pos1}<b>{pos2}</b>{pos3}
							</div>
						</li>
				);
			}else{
				variants.push(<li className={cname} onClick={function(){self.clickElement(el)}}>
							<div className="label" >
								{el.name.toLowerCase()}
							</div>
						</li>
				);
			}
		});
		if (variants.length==0){
			variants = <li className="result-item" onClick={this.close}><div className="label" ><b>"{lower_text}"</b> не найден</div></li>
		}

		var res_style={"top":-this.state.v_pos*this.props.item_height};

		return (
			<div className="select_input" style={res_style}>
				<div className="mask" onClick={this.close} onWheel={this.wheel}></div>
				<div className="select_input__body pointer" style={style}>
					<ul className="results"  onWheel={this.wheel}>
						{variants}
					</ul>
					<div className="search" >
						<input type="text" className="input" spellCheck="false" autoCapitalize="off" autoCorrect="off"  ref="inp"   onChange={this.textChange} onKeyDown={this.keyPress}/>
					</div>

				</div>
			</div>
			);
	}
}

SelectInput.defaultProps = { item_height:36 }
module.exports = SelectInput;
