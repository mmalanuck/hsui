"use strict";


require("./popup_login.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");

var Popup = require("../popup/popup.jsx");
var LoginForm = require("../login_form/login_form.jsx");
class PopupLogin extends React.Component{
	constructor(props){
		super();

	}
	render(){
		return (
			<Popup title="Регистрация репетитора" maxWidth="820">
				<LoginForm login="/login/" register="/register/" recover="/reset_pass/" />
			</Popup>
		)
	}
}


module.exports = PopupLogin;
