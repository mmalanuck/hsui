"use strict";

require("./discipline_list.less");

var React = require("react");
var $ = require("jquery");
var _ = require("underscore");
var ReactDom = require("react-dom");
var Scrollable = require("../scrollable/scrollable.jsx");
var ListItem = require("../list_item/list_item.jsx");


/*
subject
discipline
city
url
onSelect
*/
class DisciplineList extends React.Component{
	constructor(props){
		super();

		this.fetch_items = this.fetch_items.bind(this);
		this.selectItem = this.selectItem.bind(this);
		this.selectSubjects = this.selectSubjects.bind(this);
		this.parse_data = this.parse_data.bind(this);

		this.state = {
			subjects:[],
			subject: props.subject,
			disciplines:[],
			discipline: props.discipline,
			city: props.city,
			url: props.url || "/api/disciplines_for_city"
		}
	}

	componentDidMount(){
				this.fetch_items()
	}

	parse_data(data){
		var subjects = [];
		var subjects_dict = {}
		_.each(data.result, function(i){
			if (subjects_dict.hasOwnProperty(i["subject"])==false){
				subjects_dict[i["subject"]] = i["subject"];
				subjects.push({"id":i["subject"], "name":i["subject_name"]})
			}
		})
		this.subjects_list = subjects;
		this.disciplines_list = data.result;

		this.setState({
			'subjects': subjects,
			"disciplines":data.result
		});
		if (this.refs.scroll_subjs){
			this.refs.scroll_subjs.recalc_size();
		}

		if (this.refs.scroll_subjs){
			this.refs.scroll_subjs.recalc_size();
		}
	}

	fetch_items(){
		var fetch_url = this.state.url;
		if (this.state.city){
			fetch_url += "/" + this.props.city
		}
		$.ajax({
	      url: fetch_url,
	      dataType: 'json',
	      success: this.parse_data,
	      error: function(xhr, status, err) {
	        console.error('"'+status+'":', err);
	      }.bind(this)
	  	})
	}

	selectItem(item){
		this.setState({"discipline":item.value});
		var item = {"id":item.value,"name":item.name,"subject":this.state.subject};
		if (this.props.onSelect){
			this.props.onSelect(item);
		}
	}

	selectSubjects(item){
    var discs = _.filter(this.disciplines_list, function(i){return i.subject==item.value});
		var self = this;
		this.setState({
			"subject":item.value,
			"discipline":null,
			"disciplines":discs,
		});

    setTimeout(function(){
        self.refs.scroll_disciplines.recalc_size();
    },10);
	}

	render(){
		var item = null;
		var active_class = "list-item";

    var subjects = [];
		var selected = false;
		for (var i=0; i<this.state.subjects.length; i++){
			item = this.state.subjects[i];
			selected = item.id == this.state.subject;
			subjects.push(<ListItem name={item.name} value={item.id} onClick={this.selectSubjects} key={item.id} selected={selected}>{item.name}</ListItem>);
		}

		var disciplines = [];
		var disc;
		if (this.state.disciplines.length>0){
			for (var i=0; i<this.state.disciplines.length; i++){
				item = this.state.disciplines[i];
				selected = item.id == this.state.discipline;
				disc = <ListItem name={item.name} value={item.id} onClick={this.selectItem} key={item.id} selected={selected}>{item.name}</ListItem>
				// if (item.count>0){
					// disc = <div className={active_class} data-id={item.id} data-name={item.name} onClick={this.selectItem}  key={item.id}> {item.name} <span className="badge">{item.count}</span> </div>
				// }
				disciplines.push(disc);
			}
		}else{
			return (
				<div>
					<div className="row">
						<div className="col-md-12">
							Для вашего города пока нет дисциплин с зарегистрированными репетиторами
						</div>

					</div>
				</div>
				)
		}

		if (this.refs.scroll) this.refs.scroll.invalidate();
		return (
			<div>
				<div className="row">
					<div className="col-md-6">
						<Scrollable ref="scroll_subjs">
							<div className="list">
								{subjects}
							</div>
						</Scrollable>
					</div>
					<div className="col-md-6">
						<Scrollable ref="scroll_disciplines">
							<div className="list">
								{disciplines}
							</div>
						</Scrollable>
					</div>
				</div>
			</div>
		);
	}
}



module.exports = DisciplineList;
