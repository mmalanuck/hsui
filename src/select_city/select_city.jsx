"use strict";

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");

var Popup = require("../popup/popup.jsx");
var CitiesList = require("../cities_list/cities_list.jsx");


class SelectCityElement extends React.Component{
	constructor(props){
		super();

		this.showPopup = this.showPopup.bind(this);
		this.closePopup = this.closePopup.bind(this);
		this.onSelect = this.onSelect.bind(this);

		this.state = {
			"city":props.city,
			"title":props.title || "Не указано",
			"fo":props.fo,
			"region":props.region
		};
	}

	showPopup(){
		var popup = <Popup title="Выберите город" maxWidth="820" >
						<CitiesList  onSelect={this.onSelect}  fo={this.state.fo} region={this.state.region} city={this.state.city} />
					</Popup>
		this.popup = ReactDom.render(	popup, this.refs.popup);

	}

	closePopup(){
		if (this.popup){
			this.popup.remove_me();
			this.popup = null;
		}
	}

	onSelect(city){
		setTimeout(this.closePopup,10);
		var st = {};
		st.city = city.id;
		st.fo = city.fo;
		st.region = city.region;
		st.title = city.name;
		this.setState(st);
	}

	render(){
		return (
			<span className="select_city">
				<span className="select_city__title" onClick={this.showPopup}>{this.state.title}</span>
				<input type="hidden" name={this.props.name} value={this.state.city} />
				<div className="select_city__popup" ref="popup"></div>
			</span>
		);
	}
}

module.exports = SelectCityElement;
