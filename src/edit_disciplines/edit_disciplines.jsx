"use strict";

require("./edit_disciplines.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");
var PopupDisciplines = require("../popup_disciplines/popup_disciplines.jsx");
var Button = require("../button/button.jsx");
var ListItem = require("../list_item/list_item.jsx");


class EditDisciplines extends React.Component{
	constructor(props){
		super();

		this.cached = this.cached.bind(this);
		this.submit = this.submit.bind(this);
		this.open = this.open.bind(this);

		this.url = props.url || "/api/subj_disciplines";

		var selected = [];
		if (props.value){
				var splt = null;
				_.each(props.value.split(";"), function(item){
					if (item.length>=2 && item.indexOf("=")>=0){
						splt = item.split("=");
						selected.push({"name":splt[0],"id":splt[1]});
					}
				});
		}

		this.state = {
			items:null,
			selected:selected,
			value:props.value,
			popup:null,
			cache:null
		}
	}

	cached(item){
		this.setState({cache:item});
	}

	submit(data){
		var value = "";
		_.each(data,function(el){value+=";"+el.name+"="+el.id});
		value=value.slice(1);
		this.setState({value:value,selected:data});
	}

	open(){
		var popup_div = $("<div class='popups'></div>");
		$('#popup_container').append(popup_div);
		var popup = ReactDom.render(
		  	<PopupDisciplines url={this.url} forCache={this.cached} data={this.state.cache} value={this.state.selected}  onClose={this.onClose} onSubmit={this.submit} /> ,
		  popup_div[0]);
		this.popup = popup;
	}

	render(){
		var results = <span>Нет дисциплин</span>
		if (this.state.selected){
			var res_items = _.map(this.state.selected,function(el,i,lst){
				return  <ListItem key={el.id}>{el.name}</ListItem>
			})
			results= <div className="disciplinesEdit__itemList">{res_items}</div>
		}
		return(
			<div className="disciplinesEdit">
				{results}
				<Button onClick={this.open}>Изменить дисциплины</Button>
				<input type="hidden" name={this.props.name} value={this.state.value} />
			</div>
		)
	}
}

module.exports = EditDisciplines;
