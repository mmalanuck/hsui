"use strict";

require("./textarea.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");


// name
// placeholder
// readonly
class Textarea extends React.Component{
	constructor(props){
		super();

		this.onChange = this.onChange.bind(this);
		this.validate = this.validate.bind(this);

		this.state = {
			value: props.value,
			hasError: props.hasError,
			message: null
		}
	}
	validate(val){
		return true;
	}

	onChange(e){
		var value = e.target.value;
		var valid_message = this.validate(value);
		if (valid_message !=true ){
				this.setState({value: value, hasError: true, message: valid_message});
		}else{
			this.setState({value: value, hasError: false, message: null});
		}

		if (this.props.onChange){
				this.props.onChange(value);
		}
	}

	render(){
		var cls_name =  "textarea";
		if (this.state.hasError){
			cls_name +=" textarea_mod_error";
		}
		var message = null;
		if (this.state.hasError){
			message = <div className="textarea_message">{this.state.message}</div>
		}
		return (
			<div className={cls_name}>
				<textarea placeholder={this.props.placeholder} name={this.props.name} value={this.state.value} onChange={this.onChange}/>
				{message}
			</div>
		)
	}
}


module.exports = Textarea;
