"use strict";

require("./checkbox.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");
var Icon = require("../icon/icon.jsx");

/*
onClick
path
*/

class Checkbox extends React.Component{
	constructor(props){
		super();
		this.onClick = this.onClick.bind(this);

		this.state = {
			value: props.value=="true" || false
		}
	}

	onClick(){
		if (this.props.disabled){
			return;
		}
		this.setState({
			value: !this.state.value
		});
	}

	render(){
		var class_name = "checkbox";

		if (this.props.mods){
      var mods_class = _.reduce(this.props.mods.split(" "), (res, item)=>{
        return res + " checkbox__mods__"+item;
      }, "");
      class_name += mods_class;
		}

		var icon = "check-box-outline-blank";
		if (this.state.value==true){
			icon = "check-box";
		}
		if (this.props.disabled=="true"){
			class_name += " checkbox__mods__disabled";
		}

	   return (
			 <div className={class_name} onClick={this.onClick}>
			 		<Icon path={icon} no_event="true" />
			 		<input type="hidden" name={this.props.name} value={this.state.value}/>
			 </div>
		);

	}
}


module.exports = Checkbox;
