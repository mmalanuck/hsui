"use strict";

require("./select_switcher.less");
var React = require("react");
var ReactDom = require("react-dom");

class SelectSwitcher extends React.Component{
	constructor(props){
		super(props);

		this._click = this._click.bind(this)
		this.change = this.change.bind(this);

		this.state =  {
			"value": this.props.value
		};
	}

	componentWillMount(){
		if (this.props.value) this.setState({'value':this.props.value});
	}

	change(value){
		if (this.state.value == value) return;

		this.setState({value: value});
		if (this.props.onChange){
			this.props.onChange(value);
		}
	}

	_click(e){
		if (this.props.allowBlank!="true"){

			if (e.target===this.refs.first){
				this.change(0);
			}
			if (e.target===this.refs.second){
				this.change(1);
			}
		}else{
			if (e.target===this.refs.first){
				if (this.state.value==0){
					this.change(-1);
				} else {
					this.change(0);
				}
			}
			if (e.target===this.refs.second){
				if (this.state.value==1){
					this.change(-1);
				} else {
					this.change(1);
				}
			}
		}
	}

	render(){
		var select_name = "switcher__slider"
		if (this.state.value==0) select_name+=" switcher__slider__first";
		if (this.state.value==1) select_name+=" switcher__slider__second";

		return (
			<div className="switcher">
				<div className={select_name}>
					<span className="switcher__first" onClick={this._click} ref="first">{this.props.first}</span>
					<span className="switcher__second" onClick={this._click} ref="second">{this.props.second}</span>
				</div>
				<input type="hidden" name={this.props.name} value={this.state.value} />
			</div>
		)
	}
}

SelectSwitcher.defaultProps = { allowBlank: false }
module.exports = SelectSwitcher;
