"use strict";

require("./scrollable.less");

var React = require("react");
var ReactDom = require("react-dom");
var $ = require("jquery");


class Scrollable extends React.Component{
	constructor(props){
		super();

		this.pixels2percents = this.pixels2percents.bind(this);
		this.invalidate = this.invalidate.bind(this);
		this.recalc_size = this.recalc_size.bind(this);
		this.wheel = this.wheel.bind(this);
		this.prepare_click_coords = this.prepare_click_coords.bind(this);
		this.navigateLine = this.navigateLine.bind(this);
		this.dragStart = this.dragStart.bind(this);
		this.drag = this.drag.bind(this);
		this.dragEnd = this.dragEnd.bind(this);

		this.state = {
			pos: 0,
			max_height: 400,
			content_height: 0,
		}
	}
	componentDidMount(){
		this.recalc_size();
	}

	pixels2percents(val){
		var res = 100.0/this.state.max_height*val;
		if (res>100) res = 100;
		if (res<0) res = 0;
		return res;
	}

	invalidate(){
		setTimeout(this.recalc_size, 30);
	}

	recalc_size(){
		var cl_size = 401;
		cl_size =$(ReactDom.findDOMNode(this)).parent().height();
		var cont_height =$(this.refs.content)[0].clientHeight;
		this.setState({
			content_height: cont_height
		});
	}

	wheel(e){
		e.preventDefault();
		var val = 100.0/this.state.content_height*60;
		if (e.deltaY<0) {
			val = this.state.pos - val;
		}
		else {
			val = this.state.pos+ val;
		}
		if (val>100) val = 100;
		if (val <0) val = 0;

		this.setState({"pos":val});
	}

	prepare_click_coords(e){

		var base_pos = ReactDom.findDOMNode(this.refs.wrap);
		var offset_top = base_pos.offsetTop;
		return  e.clientY-offset_top-80;
	}

	navigateLine(e){
		e.preventDefault();
		this.setState({"pos":this.pixels2percents(this.prepare_click_coords(e))});
	}

	dragStart(e){
		window.addEventListener("mousemove",this.drag);
		window.addEventListener("mouseup",this.dragEnd);
	}

	drag(e){
		e.preventDefault();
		this.setState({"pos":this.pixels2percents(this.prepare_click_coords(e))});
	}

	dragEnd(e){
		window.removeEventListener("mousemove",this.drag);
		window.removeEventListener("mouseup",this.dragEnd);
	}

	render(){
		var content_height = this.state.content_height;
		var crop_style = {"height":this.state.max_height};
		var scrollbar_size = this.state.max_height;
		var display_bar = "block";
		if (scrollbar_size < content_height){
			scrollbar_size = scrollbar_size*(scrollbar_size/content_height);
		}else{
			display_bar = "none";
		}
		var scrollbar_pos = this.state.pos;
		scrollbar_pos = (this.state.max_height-scrollbar_size)/100.0*scrollbar_pos;



		var scrollbar_style = {"height":scrollbar_size, "top":scrollbar_pos, "display":display_bar};
		var content_pos = (content_height-this.state.max_height)/100.0*this.state.pos;
		if (content_pos<0) content_pos = 0;

		var content_style = {"top":-content_pos};
		var scroll_line_style={"display":display_bar};

		return(
			<div className="scroll_wrap" ref="wrap" onWheel={this.wheel} >
				<div className="crop" ref="crop" style={crop_style} >
					<div className="scroll_content" ref="content" style={content_style} >
						{this.props.children}
					</div>
					<div className="scroll" ref="line" onClick={this.navigateLine} style={scroll_line_style}></div>
				</div>

				<div className="scrollbar" ref="scrollbar" style={scrollbar_style} onMouseDown={this.dragStart}></div>
			</div>
		)
	}
}
module.exports = Scrollable;
