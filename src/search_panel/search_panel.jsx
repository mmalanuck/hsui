"use strict";

require("./search_panel.less");
var React = require("react");
var DisciplineField = require("../discipline_field/discipline_field.jsx");
var CityField = require("../city_field/city_field.jsx");
var SelectField = require("../select_field/select_field.jsx");
var Button = require("../button/button.jsx");
var RangeField = require("../range_field/range_field.jsx");
var VariantsField = require("../variants_field/variants_field.jsx");
var SelectSwitcher = require("../select_switcher/select_switcher.jsx");
var Input = require("../input/input.jsx");
var Textarea = require("../textarea/textarea.jsx");


class SearchPanel extends React.Component{
	constructor(props){
		super();

		this.onSwitcherChange = this.onSwitcherChange.bind(this);
		this.submitForm = this.submitForm.bind(this);
		this.validateForm = this.validateForm.bind(this);

    this.cost_min =  props.costMin || 0;
    this.cost_max = props.costMax ||  9000;

    var cost_a = props.costA || -1;
    var cost_b = props.costB || 6000;
    if (cost_a<this.cost_min){
      cost_a = this.cost_min;
    }
    if (cost_b>this.cost_max){
      cost_b = this.cost_max;
    }


		this.state = {
			mode: 0,
      cityId: props.cityId || 449,
      cityName: props.cityName || "Курск",
      metroName: props.metroName || "",
      metroId: props.metroId || -1,
      raionName: props.raionName || "",
      raionId: props.raionId || -1,
      disciplineName: props.disciplineName || "",
      disciplineId: props.disciplineId || -1,
      boolEge: props.boolEge ||  -1,
      boolSkype: props.boolSkype || -1,
      boolRepetitor: props.boolRepetitor || -1,
      boolStudent: props.boolStudent || -1,
      costA: cost_a,
      costB: cost_b,

			city_validation : true,
			discipline_validation : true,
			email_validation : true,
			phone_validation : true
		}
	}
	validateForm(){
		var isValid = true;
		if (this.state.mode==0){
			if (this.state.cityId == -1) {
				this.setState({city_validation : false});
				isValid = false;
			}
			if (this.state.disciplineId == -1) {
				this.setState({discipline_validation : false});
				isValid = false
			};
		}else{
			if (this.state.cityId == -1) {
				this.setState({city_validation : false});
				isValid = false;
			}
			if (this.state.disciplineId == -1) {
				this.setState({discipline_validation : false});
				isValid = false;
			}
			if (this.state.email == "") {
				this.setState({email_validation : false});
				isValid = false;
			}
			if (this.state.phone == ""){
				this.setState({phone_validation : false});
				isValid = false;
			}
		}

		return isValid;
	}
	submitForm(){
		if (this.validateForm()==false){
				console.log("FAIL!");
				this.refs.form.submit();
				console.log("OK!");
				// this.forceUpdate();
		}

	}
	onSwitcherChange(value){
		this.setState({mode: value});
	}

	render(){
    var raionUrl = "/api/raion/"+this.state.cityId+"/";
    var metroUrl = "/api/metro/"+this.state.cityId+"/";

		var panel = null;
		if (this.state.mode == 0){
			panel = <div className="row">
					<input name="action" type="hidden" value="filter_teacher" />
					<div className="col-md-3">
						<div className="field">
								<div className="form-group">
									<label className="control-label">Город:</label>
									<CityField isValid={this.state.city_validation} value={this.state.cityId} title={this.state.cityName} name='city' />
								</div>
						</div>
						<div className="field">
								<div className="form-group">
									<label className="control-label">Район:</label>
									<SelectField url={raionUrl} value={this.state.raionId} name="raion" title={this.state.raionName} />
								</div>
						</div>
						<div className="field">
								<div className="form-group">
									<label className="control-label">Метро:</label>
									<SelectField url={metroUrl} name="metro" value={this.state.metroId} title={this.state.metroName} />
								</div>
						</div>
					</div>

					<div className="col-md-3">
						<div className="field">
								<div className="form-group">
									<label className="control-label">Дисциплина:</label>
									<DisciplineField name="discipline" isValid={this.state.discipline_validation} value={this.state.disciplineId} title={this.state.disciplineName}  />
								</div>
						</div>
						<div className="field">
								<div className="form-group">
									<label className="control-label">Подготовка к ЕГЭ:</label>
									<SelectSwitcher first="Да" second="Нет" value={this.state.boolEge} allow_blank="true" name="ege"  />
								</div>
						</div>
						<div className="field">
								<div className="form-group">
									<label className="control-label">Занятия по Skype:</label>
									<SelectSwitcher first="Да" second="Нет" value={this.state.boolSkype} allow_blank="true" name="skyped"  />
								</div>
						</div>
					</div>


					<div className="col-md-3">
						<div className="field">
								<div className="form-group">
									<label className="control-label">Пол:</label>
									<VariantsField value="-1" name="gender" title="Не указано" values="gender_list_options"  />
								</div>
						</div>
						<div className="field">
								<div className="form-group">
									<label className="control-label">Занятие у преподавателя:</label>
									<SelectSwitcher first="Да" second="Нет" value={this.state.bool_repetitor} allow_blank="true" name="own_territory"  />
								</div>
						</div>
						<div className="field">
								<div className="form-group">
									<label className="control-label">Выезд к ученику:</label>
									<SelectSwitcher first="Да" second="Нет" value={this.state.bool_student} allow_blank="true" name="student_territory"  />
								</div>
						</div>
					</div>

					<div className="col-md-3">
						<div className="field">
								<div className="form-group">
									<label className="control-label">Стоимость часа:</label>
									<RangeField min={this.cost_min} max={this.cost_max} name_a="cost_min"
										 name_b="cost_max"
										 valueA={this.state.costA}
										 valueB={this.state.costB}  />
								</div>
						</div>
					</div>
			</div>
		}else{
			panel = <div className="row">
				<div className="col-md-3">
					<div className="field">
							<div className="form-group">
								<label className="control-label">Город:</label>
								<CityField isValid={this.state.city_validation} value={this.state.cityId} title={this.state.cityName} name='city' />
							</div>
					</div>
					<div className="field">
							<div className="form-group">
								<label className="control-label">Дисциплина:</label>
								<DisciplineField name="discipline" isValid={this.state.discipline_validation} value={this.state.disciplineId} title={this.state.disciplineName}  />
							</div>
					</div>
				</div>
				<div className="col-md-3">
					<div className="field">
						<div className="form-group">
							<label className="control-label">Ваш Email:</label>
							<Input name="email" hasError={!this.state.email_validation} value={this.state.email} />
						</div>
					</div>
					<div className="field">
							<div className="form-group">
								<label className="control-label">Номер телефона:</label>
									<Input name="phone" hasError={!this.state.phone_validation}  value={this.state.phone} />
							</div>
					</div>
				</div>

				<div className="col-md-6">
					<div className="field">
							<div className="form-group">
								<label className="control-label">Описание заявки:</label>
								<Textarea name="additionalInfo"/>
							</div>
					</div>
				</div>
			</div>
		}


		return (
      <div className="searchPanel">
				<form methid="POST" ref="form">
					<div className="searchPanel__switcher">
						<SelectSwitcher	 onChange={this.onSwitcherChange} first="Подбор репетитора" second="Быстрая заявка" value="0" allow_blank="true" name="test_input" />
					</div>
					{panel}
          <div className="buttonList buttonList__mod_right">
            <Button mods="success" onClick={this.submitForm}>Подобрать</Button>
          </div>
				</form>
      </div>
		)
	}
}

module.exports = SearchPanel;
