"use strict";


require("./element_generator.less");

var React = require("react");
var ReactDom = require("react-dom");
var $ = require("jquery");


class ElementGenerator extends React.Component{
	constructor(props){
		super();

		this.click = this.click.bind(this);

		this.state = {
			tag: props.tag || "div"
		}
	}
	click(){
		var el = "<"+this.state.tag;
		var properties = []
		for (var property in this.props) {
		    if (this.props.hasOwnProperty(property)) {
		    	if (property.startsWith("prop")){
		    		properties.push(property.slice(4));
		    	}

		    }
		}
		for (var i in properties){
			var prop_name = properties[i];
			el = el+ " "+prop_name+"='"+this.props["prop"+prop_name]+"'"
			// el.attr(prop_name,this.props["prop"+prop_name]);
		}
		el = el + " />"
		// if (this.props.prop_name) el.addAttr("name",this.props.prop_name);
		// if (this.props.prop_value) el.addAttr("value",this.props.prop_value);
		// if (this.props.prop_url) el.addAttr("url",this.props.prop_url);

		var j_el = $(el);

		if (this.props.target){
			$(this.props.target).append(j_el);
		}else{
			console.log("Target '"+this.props.target+"' for element_generator not founded");
			$(ReactDom.findDOMNode(this)).parent().append(j_el);
			// $("body").append(j_el);
		}


	}

	render(){
		var style = {"cursor": "pointer"}
		return(
			<span className="element_generator" style={style} onClick={this.click}>{this.props.text}</span>
		)
	}
}



module.exports = ElementGenerator;
