require("./style.css");
import registerReact from "./reactive-elements"
var React = require("react");

function register(short_name, tag_name){
	var Component = require("./"+short_name+"/"+short_name+".jsx");
	registerReact(tag_name, React.createFactory(Component));
}

register("select_switcher", "r-select_switcher");

// var SelectSwitcher = require("./select_switcher/select_switcher.jsx");
// registerReact('r-select_switcher', React.createFactory(SelectSwitcher));
register("select_input", "r-select_input");
register("select_field", "r-select_field");
register("variants_field", "r-variants_field");
register("number_range_field", "r-number_range_field");
register("scrollable", "r-scrollable");
register("popup", "r-popup");
register("element_generator", "r-element_generator");
// var PopupDisciplines = require("./popup_disciplines/popup_disciplines.jsx");
// registerReact('r-popup_disciplines', React.createFactory(PopupDisciplines));
register("image_upload", "r-image_upload");
register("tabs", "r-tabsexample");
register("admin_panel", "r-admin_panel");
register("range_field", "r-range_field");
register("list_item", "r-list_item");
register("cities_list", "r-cities_list");
register("select_city", "r-select_city");
register("city_field", "r-city_field");
register("popup_login", "r-popup_login");
register("login_form", "r-login_form");
register("register_form", "r-register_form");
register("popup_register", "r-popup_register");
register("discipline_list", "r-discipline_list");
register("discipline_field", "r-discipline_field");
register("input_element", "r-input_element");
register("disciplines_collect", "r-disciplines_collect");
register("popup_disciplines", "r-popup_disciplines");
register("button", "r-button");
register("icon", "r-icon");
register("checkbox", "r-checkbox");
register("edit_disciplines", "r-edit_disciplines");
register("search_panel", "r-search_panel");
register("input", "r-input");
register("textarea", "r-textarea");
