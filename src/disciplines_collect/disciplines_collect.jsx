"use strict";

require("./disciplines_collect.less");

var React = require("react");
var $ = require("jquery");
var _ = require("underscore");
var Scrollable = require("../scrollable/scrollable.jsx");
var ListItem = require("../list_item/list_item.jsx");
var Icon = require("../icon/icon.jsx");


class DisciplinesCollect extends React.Component{
	constructor(props){
		super();

		this.fetchFromServer = this.fetchFromServer.bind(this);
		this.selectChaper = this.selectChaper.bind(this);
		this.toggleDiscipline = this.toggleDiscipline.bind(this);
		this.changeItems = this.changeItems.bind(this);

		var chapter = null;
		if (props.data!=null) chapter = 0;

		this.state = {
			data:props.data || null,
			chapter: chapter,
			value: props.value || [],
			filtered_childs:null,
		}
	}

	componentWillMount(){
		if (this.props.data==null) this.fetchFromServer();
	}
	changeItems(items){
		this.setState({value: items});
		if (this.props.onChange){
			var event = {items: items};
			this.props.onChange(event);
		}
	}

	fetchFromServer(){
		$.ajax({
	      url: this.props.url,
	      dataType: 'json',
	      success: function(data) {
	        this.setState({'data': data.result});
	        this.selectChaper(data.result.items[0]);

	        if (this.props.forCache){
	        	this.props.forCache(data.result);
	        }
	      }.bind(this),
	      error: function(xhr, status, err) {
	        console.error(status, err.toString());
	      }.bind(this)
	  });

	}

	selectChaper(item){
		var child_items = _.filter(this.state.data.childs,function(el){return el.parent==item.id});
		this.setState({chapter:item.id,filtered_childs:child_items});
	}

	toggleDiscipline(item){
		var items = this.state.value;
		var has_item = _.filter(items,function(el){return el.id==item.id});
		if (has_item.length>0){
			items = _.filter(items,function(el){return el.id!=item.id});
		}else{
			items.push(item);
		}
		this.changeItems(items);
	}
	remove_element(item){
		var items = this.state.value;
		items = _.filter(items,function(el){return el.id!=item.id});
		this.changeItems(items);
	}

	render(){
		var subjs = null;
		var disc = null;
		if (this.state.data){
			var self = this;
			subjs = _.map(this.state.data.items,function(el,i,lst){
				var func = function(){self.selectChaper(el)};
				let key = "subjs_" + el.id;
				var selected = el.id==self.state.chapter;
				return <ListItem selected={selected} onClick={func} value={el.id} key={key} >{el.name}</ListItem>
			});

			disc = _.map(this.state.filtered_childs,function(el,i,lst){
				let is_selected = false;
				_.each(self.state.value,function(it){if (el.id==it.id) is_selected=true});

				var func = function(){self.toggleDiscipline(el)};
				let key = "disc_" + el.id;
				let decor = null;
				let icon = false;
				if (is_selected){
					decor = "success";
					icon = <Icon path="done" decoration="success"/>
				}


				return <ListItem value={el.id} mods={decor} onClick={func} key={key}>
					{icon}
					{el.name}
				</ListItem>
			})
		}
		var results = _.map(this.state.value,function(el,i,lst){
				let key = "res_" + el.id;
				var func = function(){self.remove_element(el)};
				return <div className="disciplines_collect__result_item" value={el.id} key={key} >
					{el.name}
					<Icon path="close" mods="error" onClick={func}/>
				</div>
			});

		if (this.refs.scroll1) this.refs.scroll1.invalidate();
		if (this.refs.scroll2) this.refs.scroll2.invalidate();
		if (this.refs.scroll3) this.refs.scroll3.invalidate();

		return (
			<div className="disciplines_collect">
				<div className="row">
						<div className="col-md-4">
							<div className="disciplines_collect__title">Направление:</div>
							<Scrollable ref="scroll1">
								<div className="disciplines_collect__list">{subjs}</div>
							</Scrollable>
						</div>
						<div className="col-md-4">
							<div className="disciplines_collect__title">Дисциплины:</div>
							<Scrollable ref="scroll2">
								<div className="disciplines_collect__list">{disc}</div>
							</Scrollable>
						</div>
						<div className="col-md-4">
							<div className="disciplines_collect__title">Выбрано:</div>
							<Scrollable ref="scroll3">
								<div className="disciplines_collect__list">{results}</div>
							</Scrollable>
						</div>
				</div>
			</div>
		)
	}
}

module.exports = DisciplinesCollect;
