"use strict";


require("./tabs.less");
var React = require("react");
var $ = require("jquery");
var _ = require("underscore");

var TabsModule = {};

// Пример использования
// <Tabs>
//     <TabHeader>
//         <TabHeaderItem item="quick_form" active="1">Новый заказ</TabHeaderItem>
//         <TabHeaderItem item="my_orders">Мои заказы</TabHeaderItem>
//     </TabHeader>
//     <TabItem name="quick_form">
//         <p>Форма быстрого заказа</p>
//     </TabItem>
//     <TabItem name="my_orders">
//         <p>Мои заказы</p>
//     </TabItem>
// </Tabs>

class Tabs extends React.Component{
	constructor(props){
		super();

		this.modiffed_childrens = null;

		this.change_tab = this.change_tab.bind(this);

		this.header = null;
		this.tabs = [];

		this.state = {
			selected: null
		}
	}

	componentDidMount(){
		// отображаем содержимое первой вкладки
		// ...
	}

	change_tab(item){
		var tab_item = item.props.item;
		_.each(this.tabs, function(element, index){
			element.show_tab(tab_item);
		});
	}

	render(){
		return (
			<div>
				{this.modiffed_childrens}
            </div>
		)
	}
}

class TabHeader extends React.Component{
	constructor(props){
		super();

		this.child_items = [];

		this.click_header = this.click_header.bind(this);
		this.click = this.click.bind(this);
		// this.register_in_parent = this.register_in_parent.bind(this);

		this.state = {
			selected:null
		};
	}

	componentDidMount(){
		// if (this.props.register_in_tab){
			// this.props.register_in_tab(this);
		// }
		var items = this.props.children;
		var self = this;
		// Добавляем ссылки дочерних элементов
		console.log(items);
		var res = React.Children.map(items, function(pos, item, t){
			return pos;
		})
		console.log("::",res);
		// _.each(items, function(element,index,list){
			// console.log("-->",element);
			// var set_click_function = element.type.prototype.set_click.bind(element);
			// set_click_function(self);
		// });
	}

	componentWillMount(){
		var items = this.props.children;
		var self = this;
		// Добавляем ссылки дочерних элементов
		_.each(items, function(element,index,list){
			// element.props.on_click = self.click_header;

			// self.child_items.push(element);
			// element.props.register_in_parent = self.register_in_parent;
		});
	}


	click_header(item){
		// функция вызывается из дочернегоTabHeaderItem
		// нужна для назначения кликнутого элемента свойству "selected"

		if (item !== this.state.selected){
			this.setState({selected:item});
			this.is_changed = true;
			if (this.props.change_tab){
				this.props.change_tab(item);
			}
		}else{
			this.is_changed = false;
		}
	}

	click(e){
		if (this.is_changed){
			_.each(this.child_items, function(item){
				console.log("-->>>>", item);
				// item.setState({status:0});
			});

			var self = this;
			setTimeout(function() {
				if (self.state.selected){
					self.state.selected.setState({status:1});
				}
			}, 2);
		}
	}

	register_in_parent(item){
		// функция используется для получения доступа к дочерним объектам
		this.child_items.push(item);
	}


	render(){
		return (
			<div className="tab_buttons" onClick={this.click}>{this.props.children}</div>
		)
	}
}

class TabHeaderItem extends React.Component{
	constructor(props){
		super();

		this.parent = null;

		this.click  = this.click.bind(this);
		this.set_parent  = this.set_parent.bind(this);

		this.state = {
			status: props.active
		};
	}
	componentDidMount(){
		// if (this.props.register_in_parent){
			// this.props.register_in_parent(this);
		// }
		if (this.props.active && this.props.active==1){
			if (this.props.on_click){
				this.props.on_click(this);
			}
		}
	}
	componentWillMount(){
	}

	set_parent(root){
		console.log("I set parent");
		this.parent = root;
	}
	click(){
		console.log("CLick");
		if (this.props.on_click){
			this.props.on_click(this);
		}
	}

	render(){
		var target = "tab_item_"+this.props.item;
		var style = {"cursor":"pointer"}

		if (this.state.status==1){
			return (
				<span data-target={target} data-active="active" style={style} onClick={this.click} className="button">{this.props.children}</span>
			)
		}else{
			return (
				<span data-target={target} onClick={this.click} style={style} className="button">{this.props.children}</span>
			)
		}
	}
}

class TabItem extends React.Component{
	constructor(props){
		super();

		this.show_tab = this.show_tab.bind(this);
		this.click = this.click.bind(this);

		this.state = {
			visible: false
		}
	}

	componentDidMount(){
		// if (this.props.register_in_tab){
			// this.props.register_in_tab(this);
		// }
	}

	show_tab(tab_name){
		if (tab_name == this.props.name){
			this.setState({visible: true})
		}else{
			this.setState({visible: false})
		}
	}


	click(e){
	}

	render(){
		var my_id = "tab_item_"+this.props.name;
		var style = {display: "none"};
		if (this.state.visible){
			style.display = "block";
		}
		return (
			<div className="tab" style={style} id={my_id}>{this.props.children}</div>
		)
	}
}


class TabsExample extends React.Component{
	constructor(props){
		super();
	}
	render(){
		return(
			<Tabs>
			    <TabHeader>
			        <TabHeaderItem item="quick_form" active="1">Новый заказ</TabHeaderItem>
			        <TabHeaderItem item="my_orders">Мои заказы</TabHeaderItem>
			    </TabHeader>
			    <TabItem name="quick_form">
			        <p>Форма быстрого заказа</p>
			    </TabItem>
			    <TabItem name="my_orders">
			        <p>Мои заказы</p>
			    </TabItem>
			</Tabs>
		)
	}
}

TabsModule.TabItem = TabItem;
TabsModule.TabHeader = TabHeader;
TabsModule.TabHeaderItem = TabHeaderItem;
TabsModule.Tabs = Tabs;
TabsModule.TabsExample = TabsExample;

module.exports = TabsModule;
