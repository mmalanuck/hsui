"use strict";

require("./popup_disciplines.less");

var React = require("react");
var ReactDom = require("react-dom");

var $ = require("jquery");
var _ = require("underscore");
var Popup = require("../popup/popup.jsx");
var Scrollable = require("../scrollable/scrollable.jsx");
var DisciplinesCollect = require("../disciplines_collect/disciplines_collect.jsx");
var Button = require("../button/button.jsx");



class PopupDisciplines extends React.Component{
	constructor(props){
		super();

		this.close = this.close.bind(this);
		this.submit = this.submit.bind(this);
		this.onChange = this.onChange.bind(this);

		console.log("OPNE POPUP");
		console.log("cache:", props.forCache);
		console.log("data:", props.data);
		console.log("value:", props.value);

		var chapter = null;
		if (props.data!=null) chapter = 0;

		this.state = {
			data:props.data || null,
			value: props.value || [],
		}
		console.log("====>", this.state.value);
	}

	onChange(event){
		var items = event.items;
		this.setState({value: items});
	}
	close(){
		if (this.props.onClose) this.props.onClose();
		if (this.refs.popup){
			this.refs.popup.close();
		}
		// console.log(this.refs.popup);
	}

	submit(){
		if (this.props.onSubmit) this.props.onSubmit(this.state.value);
		this.close();
	}

	render(){
		return (
		<Popup title="Выбор дисциплин"  removetop="true" ref="popup">
			<div className="disciplines__select">
				<DisciplinesCollect url={this.props.url} value={this.state.value} data={this.props.data}  onChange={this.onChange}/>
				<div className="buttonList buttonList__mod_right">
					<Button  onClick={this.close}>Отмена</Button>
					<Button mods="success" onClick={this.submit}>Выбрать</Button>
				</div>
			</div>

		</Popup>
		)
	}
}


module.exports = PopupDisciplines;
