"use strict";

require("./icon.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");

/*
onClick
path
*/

class Icon extends React.Component{
	constructor(props){
		super();
		this.onClick = this.onClick.bind(this);
	}

	onClick(){
		if (this.props.onClick){
				this.props.onClick({
					value:this.props.value,
					name: this.props.name
				});
		}
	}

	render(){
		var class_name = "icon md md-" + this.props.path;

		if (this.props.mods){
      var mods_class = _.reduce(this.props.mods.split(" "), (res, item)=>{
        return res + " icon__mods__"+item;
      }, "");
      class_name += mods_class;
		}
		if (this.props.no_event=="true"){
			class_name+=" no-event";
		}

	   return (
      <i className={class_name} onClick={this.onClick} />
		);

	}
}


module.exports = Icon;
