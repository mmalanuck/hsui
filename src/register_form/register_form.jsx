"use strict";

require("./register_form.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");

var CityField = require("../city_field/city_field.jsx");
var SelectSwitcher = require("../select_switcher/select_switcher.jsx");
var Checkbox = require("../checkbox/checkbox.jsx");
var Button = require("../button/button.jsx");

class RegisterForm extends React.Component{
	constructor(props){
		super();

		this.state = {
			"login": props.login || "/login/",
			"register": props.register || "/register/",
			"recover": props.recover || "/reset_pass/"
		}
	}
	render(){
		return (
				<div className="form">
					<div className="row form_line">
						<div className="col-md-2">
							<label className="control-label" htmlFor="reg_name">Имя</label>
						</div>
						<div className="col-md-10">
							<input id="reg_name" name="name" type="text" className="form-control" />
						</div>
					</div>

					<div className="row form_line">
						<div className="col-md-2">
							<label className="control-label" htmlFor="reg_secname">Фамилия</label>
						</div>
						<div className="col-md-10">
							<input  id="reg_secname" name="secondname" type="text" className="form-control" />
						</div>
					</div>

					<div className="row form_line">
						<div className="col-md-2">
							<label className="control-label" htmlFor="reg_thirdname">Отчество</label>
						</div>
						<div className="col-md-10">
							<input  id="reg_thirdname" name="thirdname" type="text" className="form-control" />
						</div>
					</div>


					<div className="row form_line">
						<div className="col-md-2">
							<label className="control-label" htmlFor="reg_email">Email</label>
						</div>
						<div className="col-md-10">
							<input id="reg_email" name="email" type="text" className="form-control" />
						</div>
					</div>

					<div className="row form_line">
						<div className="col-md-2">
							<label className="control-label" >Город</label>
						</div>
						<div className="col-md-10">
							<CityField name="city" city={this.state.city_id} title={this.state.city_name}  region={this.state.region_id} fo={this.state.fo_id} />
						</div>
					</div>

					<div className="row form_line">
						<div className="col-md-2">
							<label className="control-label" >Роль</label>
						</div>
						<div className="col-md-10">
							<SelectSwitcher name="role" value="2" first="Репетитор" second="Студент" allow_blank="true" />
						</div>
					</div>

					<div className="row form_line">
						<div className="col-md-10 col-md-offset-2">
							<Checkbox  name="accept_tos" />
							<label className="control-label" htmlFor="reg_accept">Я принимаю пользовательские соглашения</label>
						</div>
					</div>
		</div>
		)
	}
}


module.exports = RegisterForm;
