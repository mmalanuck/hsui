"use strict";

require("./variants_field.less");

var React = require("react");
var ReactDom = require("react-dom");
var $ = require("jquery");
var SelectInputElement = require("../select_input/select_input.jsx");
var Icon = require("../icon/icon.jsx");

class VariantsField extends React.Component{
	constructor(props){
		super();

		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
		this.setValue = this.setValue.bind(this);
		this.changed = this.changed.bind(this);
		this.cancel = this.cancel.bind(this);
		this.clear = this.clear.bind(this);


		this.state = {
			is_closed:true,
			value: props.value || -1,
			title: props.title || "Не выбрано",
			data:[],
			popup_element:null,
			enabled:true
		}
	}

	componentDidMount(){
		var options = this.props.values;
		var title = undefined;
		var value = this.state.value;
		var data = $.map($("#"+options).children(), function(item){
			if (item.value == value){
				title = item.title
			}
			return {"value": item.value, "name": item.text};
		});


		var data = $.map($("#"+options).children(), function(item){
			if (item.value == value){
				title = item.title
			}
			return {"value": item.value, "name": item.text};
		});
		var st = {data: data};
		if (title){
			st.title = title;
		}
		this.setState(st);
		this.item_height = ReactDom.findDOMNode(this.refs.choice).offsetHeight;
	}

	open(){
		var	 this_node = $(ReactDom.findDOMNode(this));
		var offset = this_node.offset();
		var left = offset.left;
		var top = offset.top;
		var width = this_node.width();
		var data = this.state.data;
		var choice_node = ReactDom.findDOMNode(this.refs.choice);
		this.item_height = choice_node.offsetHeight;


		var value = this.state.value;

		var popup = ReactDom.render(
		  	<SelectInputElement item_height={this.item_height} left={left} top={top} width={width} data={data} value={value} title={this.state.title} onChange={this.changed} onCancel={this.cancel} />,
		  	document.getElementById('popup_container')
		);

		this.setState({popup_element: popup});
		if (this.state.is_closed){
			this.setState({is_closed: false});
			this.width = this_node.width();
			this.left = 0;
			this.top = $(choice_node).height()+4;
		}
	}

	close(){
		if (!this.state.is_closed){
			this.setState({is_closed: true});
		}
		if (this.state.popup_element){
			var node = ReactDom.findDOMNode(this.state.popup_element);
  			ReactDom.unmountComponentAtNode(node.parentNode);
			$(node).remove();
			this.setState({popup_element: null});
		}
	}

	setValue(val){
		this.setState({value: val});
	}

	changed(item){
		this.close();
		this.setState({title: item.name,value: item.value});
		if (this.props.onChange) this.props.onChange(item);
		if (this.props.emit){
			$(window).trigger(this.props.emit,item);
		}
	}

	cancel(){
		this.close();
	}

	clear(e){
		e.stopPropagation();
		this.setState({
			is_closed: true,
			value: -1,
			title: this.props.title || "Не выбрано"
		});
		if (this.props.emit){
			$(window).trigger(this.props.emit,{"name": "","id": null});
		}
	}

	render(){
		if (!this.state.enabled){
			return <div><label className="control-label"> --- </label></div>;
		}
		var clr = false;
		var icon_expand = <Icon path="expand-more" />;
		if (this.state.value && this.state.value!=-1){
			clr = <a className="variantsfield__icon pointer" onClick={this.clear} title="Очистить"><i className="md md-clear"></i></a>
			icon_expand = false;
		}
		return (
			<div className="variantsfield" tabIndex="0" data-enabled={this.state.enabled}>
				<div className="variantsfield__choicewrapper pointer">
				<span className="variantsfield__choice" onClick={this.open} ref="choice">
					<span className="variantsfield__text">{this.state.title}</span>
					{icon_expand}
				</span>

				{clr}
				</div>

				<input id={this.props.id} name={this.props.name} value={this.state.value} type="hidden" />
				{this.props.children}
			</div>

			);
	}
}

module.exports = VariantsField;
