"use strict";

require("./cities_list.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");

var ListItem = require("../list_item/list_item.jsx");
var Scrollable = require("../scrollable/scrollable.jsx");

/**
fo
region
city
url
link
onSelect
*/

class CitiesList extends React.Component{
	constructor(props){
		super();

		this.parse_data = this.parse_data.bind(this);
		this.filter_cities_for_region = this.filter_cities_for_region.bind(this);
		this.filter_regions_for_fo = this.filter_regions_for_fo.bind(this);
		this.recalc_all_sizes = this.recalc_all_sizes.bind(this);
		this.fetch_items = this.fetch_items.bind(this);
		this.selectItem = this.selectItem.bind(this);
		this.selectFO = this.selectFO.bind(this);
		this.selectRegion = this.selectRegion.bind(this);

		this.all_regions = null;
		this.all_cities = null;


		this.state = {
			"fos":[],
			"fo": props.fo,
			"regions":[],
			"region": props.region,
			"cities":[],
			"value": props.city,
			"data": []
		}
	}

	componentDidMount(){
		this.fetch_items()
	}

	parse_data(items){
		var fos = [];
		var fos_hash = {};
		var regions = [];
		var regions_hash = {};
		var current_city = null;

		var self = this;
		_.each(items, function(item){
			if (fos_hash.hasOwnProperty(item.fo_id)==false){
				fos_hash[item.fo_id]=item.fo_id;
				fos.push({
					id: item.fo_id,
					name: item.fo_name
				});
			}

			if (regions_hash.hasOwnProperty(item.region_id)==false){
				regions_hash[item.region_id] = item.region_id;

				regions.push({
					id: item.region_id,
					name: item.region_name,
					"fo_id": item.fo_id
				});
			}
			if (item.id == self.state.value){
				current_city = item;
			}
		});

		this.all_cities = items;
		this.all_regions = regions;

		var st = {};
		st.fos = fos;

		if (current_city){
			st.fo = current_city.fo_id;
			st.region = current_city.region_id;
		}else{
			st.fo = null;
		}
		st.cities = this.filter_cities_for_region(st.region);
		st.regions = this.filter_regions_for_fo(st.fo);
		this.setState(st);
	}

	// берём список городов для конкретного региона
	filter_cities_for_region(id){
		var cities = [];
		_.each(this.all_cities, function(i){
			if (i.region_id == id){
				cities.push(i);
			}
		});
		return cities;
	}

	// Получаем список регионов с для конкретного федерального округа
	filter_regions_for_fo(fo_id){
		var regions = [];
		_.each(this.all_regions, function(i){
			if (i.fo_id == fo_id){
				regions.push(i);
			}
		});
		return regions;
	}

	recalc_all_sizes(){
		setTimeout(function() {
			this.refs.scroll_fo.recalc_size();
        	this.refs.scroll_cities.recalc_size();
        	this.refs.scroll_regions.recalc_size();
		}.bind(this), 10);
	}

	fetch_items(){
		if (this.props.url==undefined){
			console.log("(ОШИБКА): должно быть указано свойство 'this.props.url'");
		}else{
			$.ajax({
		      url: this.props.url,
		      dataType: 'json',
		      success: function(data) {
		      	this.parse_data(data.result);
		      	this.recalc_all_sizes();
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(status, err.toString());
		      }.bind(this)
		  	})
		}
	}


	selectItem(item){
		this.setState({value: item.value});
		var result = {"id":item.value,"name":item.name,"fo":this.state.fo,"region":this.state.region};
		if (this.props.link){
			window.location = this.props.link+item.id+"?"+"back="+"'"+window.location.pathname+window.location.search+"'";
		}
		if (this.props.onSelect) this.props.onSelect(result);
	}

	selectFO(item){
		var regions = this.filter_regions_for_fo(item.value);
		this.setState({fo: item.value,value: null,"cities":[], "regions":regions, region: null});
    this.recalc_all_sizes();
	}

	selectRegion(item){
		var cities = this.filter_cities_for_region(item.value);
		this.setState({region: item.value,value: null,"cities":cities});
    this.recalc_all_sizes();
	}

	render(){
		var item = null;
		var fos = [];
		var list = null;
		for (var i=0; i<this.state.fos.length; i++){
			item = this.state.fos[i];
			list = <ListItem selected={item.id==this.state.fo} onClick={this.selectFO} value={item.id} name={item.name} key={"fo_"+item.id} >{item.name}</ListItem>
			fos.push(list);
		}

		var regions = [];
		for (var i=0; i<this.state.regions.length; i++){
			item = this.state.regions[i];
			list = <ListItem selected={item.id==this.state.region} onClick={this.selectRegion} value={item.id} name={item.name} key={item.id} >{item.name}</ListItem>
			regions.push(list);
		}
		var cities = [];
		for (var i=0; i<this.state.cities.length; i++){
			item = this.state.cities[i];
			list = <ListItem selected={item.id==this.state.value} onClick={this.selectItem} value={item.id} name={item.name} key={item.id} >{item.name}</ListItem>
			cities.push(list);
		}

		if (this.refs.scroll) this.refs.scroll.invalidate();

		return (
			<div className="cities_list">
				<div className="row">
					<div className="col-md-4">
						<div className="cities_list__title">Округ:</div>
						<Scrollable ref="scroll_fo">
							<div className="list">{fos}</div>
						</Scrollable>
					</div>
					<div className="col-md-4">
						<div className="cities_list__title">Область, республика, край:</div>
						<Scrollable ref="scroll_regions">
							<div className="list">{regions}</div>
						</Scrollable>
					</div>
					<div className="col-md-4">
						<div className="cities_list__title">Город:</div>
						<Scrollable ref="scroll_cities">
							<div className="list">{cities}</div>
						</Scrollable>
					</div>
				</div>
			</div>
		);

	}
}

CitiesList.defaultProps = { "url":"/api/full_cities" }

module.exports = CitiesList;
