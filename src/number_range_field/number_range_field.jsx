"use strict";

require("./number_range_field.less");

var React = require("react");
var ReactDom = require("react-dom");
var _ = require("underscore");
var $ = require("jquery");
var SelectInput = require("../select_input/select_input.jsx");
var Icon = require("../icon/icon.jsx");


class NumberRangeField extends React.Component{
	constructor(props){
		super();

		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
		this.changed = this.changed.bind(this);
		this.cancel = this.cancel.bind(this);
		this.clear = this.clear.bind(this);


		this.state =  {
			is_closed:true,
			value:"",
			text:"",
			data:null,
			popup_element:null
		}
	}

	componentWillMount(){
		var value = null;
		if (this.props.value) value = this.props.value;
		var data = this.generate_list(this.props.start || 0, this.props.end || 100);
		value= value|0;
		if (value==-1) value="";
		this.setState({value:value, data:data});
	}

	generate_list(start,end){
		start = start|0;
		end = end|0;
		var res = _.map(_.range(start,end),function(num){
			return {name:num.toString(),id:num};
		});
		return res;
	}

	open(){
		var dom_node = $(ReactDom.findDOMNode(this));
		var offset = dom_node.offset();
		var left = offset.left;
		var top = offset.top;
		var width = dom_node.width();
		var popup = ReactDom.render(
		  	<SelectInput left={left} top={top} width={width} data={this.state.data} value={this.state.value} title={this.state.value.toString()} onChange={this.changed} onCancel={this.cancel} />,
		  document.getElementById('popup_container')
		);
		this.setState({popup_element:popup});

		if (this.state.is_closed){
			this.setState({is_closed:false});
			this.width = dom_node.width();
			this.left = 0;
			this.top = $(ReactDom.findDOMNode(this.refs.choice)).height()+4;
		}
	}

	close(){
		if (!this.state.is_closed){
			this.setState({is_closed:true});
		}
		if (this.state.popup_element){
			var node = ReactDom.findDOMNode(this.state.popup_element);
  			ReactDom.unmountComponentAtNode(node.parentNode);
			$(node).remove();
			this.setState({popup_element:null});
		}
	}

	changed(item){
		this.close();
		this.setState({value:item.id});
		if (this.props.onChange) this.props.onChange(item);
		if (this.props.emit){
			$(window).trigger(this.props.emit,item);
		}
	}

	cancel(){
		this.close();
	}

	clear(){
		this.setState({
			is_closed:true,
			value:"",
			text:"",
		});
		if (this.props.emit){
			$(window).trigger(this.props.emit,{"name":"","id":null});
		}
	}

	render(){
		var arrow_icon = false;
		// var clr = <i className="numberrange__icon md md-expand-more"></i>;

		var icon = null;

		if (this.state.value && this.state.value!=-1){
			icon = <a className="variantsfield__icon pointer" onClick={this.clear} title="Очистить"><Icon path="clear" /></a>
		}else{
			icon = <Icon path="expand-more" no_event="true" />;
		}

		return (
			<div className="numberrange_wrapper" tabIndex="0" data-enabled={this.state.enabled}>
				<div className="numberrange__choicewrapper">
					<div className="numberrange__choice">
						<a className="numberrange__link" onClick={this.open} ref="choice"></a>
						<span className="numberrange__text">{this.state.value}</span>

						{icon}
				</div>
				</div>
				<input id={this.props.id} name={this.props.name} value={this.state.value} type="hidden" />
			</div>
			);
	}
}
module.exports = NumberRangeField;
