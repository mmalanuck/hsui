"use strict";

require("./popup.less");

var React = require("react");
var ReactDom = require("react-dom");
var $ = require("jquery");

class Popup extends React.Component{
	constructor(props){
		super();

		this.remove_me = this.remove_me.bind(this);
		this.close = this.close.bind(this);
		this.update_pos = this.update_pos.bind(this);
		this.state = {
			top:0
		};
	}

	componentDidMount(){
		setTimeout(this.update_pos,10);
		var self = this;
		this.updt_func = function(){self.update_pos()}
		$(window).resize(this.updt_func);
	}

	remove_me(){
		var node = ReactDom.findDOMNode(this);
		$(window).off("resize", this.updt_func);
		if (this.props.onClose){
			this.props.onClose();
		}
		var parent_node = node.parentNode;
		ReactDom.unmountComponentAtNode(node.parentNode);

		if (this.props.removetop=="true"){
			$(parent_node).remove();
		}else{
			$(node).remove();
		}


	}

	close(event){
		var popup_element = ReactDom.findDOMNode(this.refs.popup);
		var close_element = ReactDom.findDOMNode(this.refs.close_btn);
		if (event){
			if ((event.target===popup_element) || (event.target===close_element)){
				event.stopPropagation();
				this.remove_me();
			}
		}else{
			this.remove_me();
		}
	}

	update_pos(){
		var top = 30;
		var size = 0;
		if (this.refs.wnd){
			size = ReactDom.findDOMNode(this.refs.wnd).clientHeight;
		}
		if (size>0) top = (window.innerHeight-size)*0.25;
		this.setState({top:top});
	}

	render(){
		var popup_style={top:this.state.top};
		if (this.props.maxWidth){
			popup_style.maxWidth = this.props.maxWidth;
		}
		return (
			<div className="popup">
				<div className="popup__back" ref="popup" onClick={this.close}>
					<div className="popup__window"  style={popup_style} ref="wnd">
						<div className="popup__title">{this.props.title}</div>
						<div className="popup__close" onClick={this.close} ref="close_btn"></div>
						<div className="popup__content">
							{this.props.children}
						</div>
					</div>
				</div>
			</div>
		);
	}
}


module.exports = Popup;
