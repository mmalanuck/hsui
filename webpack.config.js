var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
var UglifyJsPlugin = require("webpack/lib/optimize/UglifyJsPlugin");

var __dirname = "D:/develop/hstutors/ui-elements"
var bower_dir = __dirname + "/bower_components";


var config = {
    addVendor: function (name, path) {
        this.resolve.alias[name] = path;
        this.module.noParse.push(new RegExp(path));
    },
    devtool: "#inline-source-map",
    entry: {
        app:"./src/boundle.js",
        vendors: ['react', 'react-dom', "jquery", "underscore"]
    },
    resolve: { alias: {} },
    output: {
        path: __dirname,
        filename: "./assets/bundle.js"
    },
    plugins:[
        new ExtractTextPlugin('./assets/style.css', {
            allChunks: true
        }),
        new CommonsChunkPlugin('vendors', './assets/vendors.js'),
        // new UglifyJsPlugin({
        //     compress: {
        //         warnings: false
        //     }
        // })
    ],
    module: {   
        noParse: [],     
        loaders: [
            { test: /\.css$/,
                loader: ExtractTextPlugin.extract('css')
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract('css!less')
            },
            { 
                test: /\.js$/,
                loader: "babel",
            },
            { 
                test: /\.jsx$/,
                loaders: ["jsx", "babel"],
                exclude: /node_modules/
            },
            {
                test: /\.jpg$/,
                loader: "url-loader"
            },
            {   
                test: /\.png$/,
                loader: "url-loader?limit=100000"
            },
      
        ]
    }
};

config.addVendor('react', bower_dir + '/react/react.js');
config.addVendor('react-dom', bower_dir + '/react/react-dom.js');
config.addVendor('jquery', bower_dir + '/jquery/dist/jquery.js');
config.addVendor('underscore', bower_dir + '/underscore/underscore.js');


module.exports = config;